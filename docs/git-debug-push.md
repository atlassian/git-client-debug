% git-debug-push(1) User manual
% Atlassian
% February, 2013

# NAME

git debug-push - Debug wrapper for the git push command

# SYNOPSIS

git debug-push [...] `repository`

# DESCRIPTION

git debug-push is a wrapper script for the push command that can be used to
log valuable debug information to a local log file named
*atlassian-stash-git-debug.log*.

It captures client side debug information by setting the `GIT_TRACE`,
`GIT_TRACE_PACKET` and `GIT_CURL_VERBOSE` environment variables. Debug output
will be writte to stdout and a pre-defined log file.

To ensure that the log file can safely be attached to a support ticket or
copied into public forums, the Authorization header of the HTTP request (if the
smart HTTP transport is being used) will be stripped and _not_ written to the
log file.

The command accepts the same options as `git push` and simply passes them
through. The basic usage boils down to using `git debug-push` instead of `git
clone`.

    git debug-push origin master
    
In addition to the debug information provided by the git push command, the debug wrapper captures valuable statistics about the local repository. This includes:

* The approximate size of the repository
* The number of contributors
* The number of commits
* The number of objects
* The number of files
* The number of refs
* And the "age" of the repository based on the first commit

# SEE ALSO

The source code may be downloaded from 
<https://bitbucket.org/atlassian/git-client-debug>.

